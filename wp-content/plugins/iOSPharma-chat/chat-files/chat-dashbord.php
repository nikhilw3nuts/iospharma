<link rel='stylesheet' href='<?php echo plugins_url().'/iOSPharma-chat/css/bootstrap.min.css' ?>' type='text/css'  />
<link rel='stylesheet' href='<?php echo plugins_url().'/iOSPharma-chat/css/admin_chat.css' ?>' type='text/css'  />
<br/><br/><div id="wrapper">
<div id="page-wrapper" class="chat-list-main" style="min-height: 541px;">
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading"> User List </div>
		</div>
		<div class="panel-content">
		<?php global $wpdb;
			$tablename=$wpdb->prefix.'chat';
			$select="SELECT * FROM `$tablename` where `user_id`!=1 group by `user_id`";
			$fields = $wpdb->get_results($select);	
		?>	<input type="hidden" name="tot_chat_user" value="<?php echo count($fields); ?>" class="tot_chat_user">
			<ul class="chack_new">
			<?php foreach($fields as $field)
				  {
					  $user_info = get_userdata($field->user_id);
					 /* echo 'User roles: ' . implode(', ', $user_info->roles) . "\n"; */ ?>
					<li><a href="<?php echo admin_url(); ?>/admin.php?page=chat-plugin&chat_id=<?php echo $field->user_id; ?>"><?php echo $user_info->user_login; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<?php 
	$tablename=$wpdb->prefix.'chat';
	$select_chat=$wpdb->get_results("SELECT * FROM `$tablename` where user_id='".$_GET['chat_id']."'");
	$total_chat=count($select_chat);
	?>
	<div class="col-lg-8">
		<div class="row">
			<!--h1 class="page-header">Chat History</h1-->
				<div class="panel panel-default">
					<div class="panel-heading">
					<ul>
						<li><b>User :</b> Username</li>
						 
					</ul>
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="chat-block">
							<div id="chat-block" class="chat-block-design">
								<div id="replace">
								<?php 
								if($total_chat>0){
								foreach($select_chat as $chat){
									if($chat->author=='user'){ $user_info = get_userdata($chat->user_id); }else{ $user_info = get_userdata($chat->admin_id);  }
										
								?>
									<div id="msgID" class="<?php echo $chat->author; ?> chat-row">
										<!--div class="user-image"><span><img src="images/avatar-icon.png"></span></div-->
										 <div class="chat-name"><span><?php echo $user_info->user_login; ?></span></div>
										<div class="message-date-row"><div class="message"><?php echo $chat->chat_text; ?></div>
										<div class="date"><?php echo $chat->date_time; ?></div></div>
									</div>
								<?php } 
								}else{ ?>
									
								<tr><td align="center" colspan="4"><b style="color:red;">No Message Found</b></td></tr>
								<?php } ?>
								</div>
							</div>
							<form enctype="multipart/form-data" method="post" action="" class="chat-form uploadimage" id="uploadimage">
								<div class="form-row">
									<div class="form-group">											
										<input type="text" value="" placeholder="message" class="form-control" id="rep_message" name="rep_message">
										<?php 
										$uid=$_GET['chat_id'];
										if($_GET['chat_id']=='')
										{
											$uid=$current_user->ID;
										}
											?>
										<input type="hidden" value="<?php echo $uid; ?>" class="form-control" id="userID" name="userID">
										<input type="hidden" value="<?php echo $current_user->ID; ?>" class="form-control" id="admid" name="admid">
										<input type="hidden" value="<?php echo $current_user->ID; ?>" name="get_list" id="get_list">
										<input type="submit" class="submit" value="">
									</div>	
									<input type="file" data-show-preview="false" class="file hide-button" name="rep_img" id="rep_img">
									<div style="cursor:pointer" class="upload-btn" id="upfile1"></div>
								</div>
							</form>
							</div>
						</div>
					</div>
				<!-- /.panel -->
			</div>
		</div>
	</div>
</div>
</div>
<script>
jQuery("body").delegate('.uploadimage','submit',function(e) 
{
e.preventDefault();

var message = jQuery('#rep_message').val();
var userID = jQuery('#userID').val();
var chaturl = "<?php echo plugins_url() ?>/iOSPharma-chat/ajax/addchat.php";
	if(message != "")
	{
		jQuery.ajax({
			type: 'POST',
			url:chaturl,
			data: new FormData(this),
			cache: false,
			contentType: false,
			processData:false,    
			success: function(data2)
			{
				//alert(data2);
				jQuery('#rep_message').val("");
				jQuery('#uploadimage').trigger("reset");
				jQuery('#chat-block').scroll();
				jQuery("#chat-block").animate({ scrollTop: 2000000000 }, 1000);	
				jQuery('#replace').append(data2); 
				
			}
		});
	}
});
function get_chat() 
{
	var userID = jQuery('#userID').val();
	if(userID != "")
	{ 
		var getChatUrl = "<?php echo plugins_url() ?>/iOSPharma-chat/ajax/get_chat.php";
		jQuery.ajax({
			type: 'POST',
			url:getChatUrl,
			data: {userID:userID},
			success: function(data2)
			{
				r = data2.split("##");
				if(r[0] !='')
				{
					jQuery('#replace').html(r[0]);
				}

				var usercountval = jQuery('.tot_chat_user').val();
				if(r[1] > usercountval){
					jQuery('.chack_new').html(r[2]);
					jQuery('.tot_chat_user').val(r[1]);
				}
			}
		});
	}
} 
setInterval(function() 
{
	 get_chat() ;
},3000);
</script>