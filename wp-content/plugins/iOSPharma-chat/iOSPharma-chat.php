<?php
/*
Plugin Name: iOSPharma-chat
Description: A iOSPharma-chat plugin.
Author: Darpan Bhatt
Version: 4.4
*/
        

register_activation_hook( __FILE__, 'my_plugin_activation' );
function my_plugin_activation() {
  add_option( 'my_plugin_activated', time() );
}

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'chat';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		`chat_id` int(11) NOT NULL AUTO_INCREMENT,
	  `user_id` int(11) NOT NULL,
	  `chat_text` varchar(255) NOT NULL,
	  `author` varchar(32) NOT NULL,
	  `date_time` datetime NOT NULL,
	  `ticket_id` int(11) NOT NULL,
	  `fileuploaded` text,
	  `admin_id` int(11) NOT NULL,
	  PRIMARY KEY (`chat_id`)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}

function jal_install_data() {
	global $wpdb;
	
	$welcome_name = 'Mr. WordPres';
	$welcome_text = 'Congratulations, you just completed the installation!';
	
	$table_name = $wpdb->prefix . 'liveshoutbox';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'time' => current_time( 'mysql' ), 
			'name' => $welcome_name, 
			'text' => $welcome_text, 
		) 
	);
}

register_activation_hook( __FILE__, 'jal_install' );
register_activation_hook( __FILE__, 'jal_install_data' );




//add chat menu
add_action('admin_menu', 'test_plugin_setup_menu');
function test_plugin_setup_menu(){
        add_menu_page( 'Chat plugin', 'Chat', 'manage_options', 'chat-plugin', 'chat_plugin_function' );
}

 
function chat_plugin_function(){
	global $current_user;
	get_currentuserinfo();
	
        include("chat-files/chat-dashbord.php");
}

?>