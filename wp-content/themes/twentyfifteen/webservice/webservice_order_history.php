<?php 
/**
 * Template Name: Order History register
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 *
 */
global $wpdb;
$user=get_current_user_id();
$orders = get_posts( array(
        'post_type'   => 'shop_order',
		'posts_per_page' => -1,
        'post_status' => 'publish',
        'tax_query'   => array( array(
                'taxonomy' => 'shop_order_status',
                'field'           => 'slug',
                'terms'         => array( 'processing', 'completed' )
        ) )
) );
//print_r($orders);

foreach($orders as $order)
{	
	if(get_post_meta($order->ID,'_customer_user',true)==$user)
	{ 
		$results = $wpdb->get_row( "SELECT * FROM `wp_woocommerce_order_items` where order_id='".$order->ID."'");
		$results->order_item_id;
		$product_id=wc_get_order_item_meta( $results->order_item_id, '_product_id', true );
		$jsonarray[]['title']=get_the_title($product_id);
		$jsonarray[]['subtitle']=get_post_meta( $product_id, 'sub_title', true );
		$jsonarray[]['dosage']=get_post_meta( $product_id, 'dosage', true );
		$jsonarray[]['tablet_quentity']=get_post_meta( $product_id, 'tablet_quentity', true );
	} 
}
$set['order']=$jsonarray;
 echo json_encode($set);
?>