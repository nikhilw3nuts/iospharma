<?php 
/**
 * Template Name: WebService FAQ 
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 * */
?>
<div class="bs-example">
<div id="accordion" class="panel-group">
<?php
// check if the repeater field has rows of data
if( have_rows('faq') ):

 	// loop through the rows of data
	$i=1;
    while ( have_rows('faq') ) : the_row();
	    // display a sub field value
      // $questions[] = get_sub_field('questions');
		//$answers[] = get_sub_field('answer');
	 ?>   

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>"><?php echo get_sub_field('questions'); ?></a>
            </h4>
        </div>
        <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <?php echo get_sub_field('answer'); ?>
            </div>
        </div>
    </div>    

<?php   $i++; endwhile;

else :

    // no rows found

endif;
		
?>
</div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
</style>
