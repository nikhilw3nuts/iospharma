<?php 
/**
 * Template Name: WebService Family Member 
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 * */
?>
<?php 

$user_id = $_GET['user_id'];
$first_name = $_GET['first_name'];
$last_name = $_GET['last_name'];
$birthday = $_GET['birthday'];
$gender = $_GET['gender'];

$post_title = $first_name.' '.$last_name;

$my_post = array(
    'post_title'    => $post_title,
    'post_status'   => 'publish',
    'post_author'   => $user_id,
	'post_type'     => 'familys'
);

$post_id = wp_insert_post( $my_post );

if($post_id == "")
{
	$results_array['sMessage'] = 'Faild.';
	$results_array['sStatus'] = '1';
	echo json_encode($results_array);
	exit;
}
else
{
	update_post_meta($post_id,'first_name',$first_name);
	update_post_meta($post_id,'last_name',$last_name);
	update_post_meta($post_id,'birthdate',$birthday);
	update_post_meta($post_id,'gender',$gender);
	$results_array['sMessage'] = 'Family Member Successfully added.';
	$results_array['sStatus'] = '0';
	$results_array['famaly_member_id'] = $post_id;
	$results_array['first_name'] = $first_name;
	$results_array['last_name'] = $last_name;
	$results_array['birthday'] = $birthday;
	$results_array['gender'] = $gender;
	echo json_encode($results_array);
	exit;	
	
}
?>






