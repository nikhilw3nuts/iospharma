<?php 
/**
 * Template Name: WebService register
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 *
 */
?>
<?php
$action = $_REQUEST['action'];

if($action == "signup1")
{
	$usernmae=$_REQUEST['user_email'];
	$email=$_REQUEST['user_email'];
	$pass=$_REQUEST['user_pass'];
	$mobile = $_REQUEST['mobile'];
	$zipcode = $_REQUEST['zipcode'];

	if(!isset($email) || $email=="")
	{
		$results_array['sMessage'] = 'invalid email.'; // Invalid Email Address
		$results_array['sStatus'] = '1';//1=fail
		echo json_encode($results_array);
		exit;
	}
	elseif(!isset($pass) || $pass=="")
	{
		$results_array['sMessage'] = 'invalid password.'; // Invalid Email Address
		$results_array['sStatus'] = '1';//1=fail
		echo json_encode($results_array);
		exit;
	}
	else
	{
		if(email_exists($email) == false)
		{
			$user_id = wp_create_user( $usernmae, $pass, $email );
			if($user_id != "")
			{
				$results_array['sMessage'] = 'success'; // Invalid Email Address
				$results_array['sStatus'] = '0';//1=fail	
				$user_id_role = new WP_User($user_id);
				$user_id_role->set_role('customer');
				update_user_meta( $user_id, 'mobile_number', $mobile );
				update_user_meta( $user_id, 'zip_code', $zipcode );
				$user = get_user_by( 'id', $user_id);
				$results_array['sMessage'] = 'success'; // Invalid Email Address
				$results_array['sStatus'] = '0';//1=fail
				$results_array['user_id'] =$user->data->ID;
				$results_array['user_email'] =$user->data->user_email;
				$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
				$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
				echo json_encode($results_array);
				exit; 
			}
			else
			{
				$results_array['sMessage'] = 'User not created.'; // Invalid Email Address
				$results_array['sStatus'] = '1';//1=fail
				echo json_encode($results_array);
				exit;
			}
		}
		else
		{
			$results_array['sMessage'] = 'User allready exist.'; // Invalid Email Address
			$results_array['sStatus'] = '1';//1=fail
			echo json_encode($results_array);
			exit;
		}
	}	
}
elseif($action == "signup2")
{
	$firstname=$_REQUEST['firstname'];
	$lastname = $_REQUEST['lastname'];
	$birthdate = $_REQUEST['birthdate'];
	$gender = $_REQUEST['gender'];	
	$user_id = $_REQUEST['user_id'];
	update_user_meta( $user_id, 'first_name', $firstname );
	update_user_meta( $user_id, 'last_name', $lastname );
	update_user_meta( $user_id, 'birth_date', $birthdate );
	update_user_meta( $user_id, 'gender', $gender );
	update_user_meta( $user_id, 'display_name', $firstname.''.$lastname );
	$results_array['sMessage'] = 'success'; // Invalid Email Address
	$results_array['sStatus'] = '0';//1=fail
	$user = get_user_by( 'id', $user_id);
	$results_array['user_id'] =$user->data->ID;
	$results_array['user_email'] =$user->data->user_email;
	$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
	$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
	$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
	$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
	$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
	$results_array['gender'] = get_user_meta($user_id,'gender',true);
	echo json_encode($results_array);
	exit;
} 
elseif($action == "signup3")
{
	$refill = $_REQUEST['refill'];
	$askdoctor = $_REQUEST['askdoctor'];
	$user_id = $_REQUEST['user_id'];
	update_user_meta( $user_id, 'would_you_like', $refill );
	update_user_meta( $user_id, 'have_you_ask', $askdoctor );
	$user = get_user_by( 'id', $user_id);
	$results_array['sMessage'] = 'success'; // Invalid Email Address
	$results_array['sStatus'] = '0';//1=fail
	$results_array['user_id'] =$user->data->ID;
	$results_array['user_email'] =$user->data->user_email;
	$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
	$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
	$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
	$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
	$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
	$results_array['gender'] = get_user_meta($user_id,'gender',true);
	$results_array['like_refill'] = get_user_meta($user_id,'would_you_like',true);
	$results_array['ask_doctor'] = get_user_meta($user_id,'have_you_ask',true);
	echo json_encode($results_array);
	exit;
}
elseif($action == "signup4")
{
	$usingfor = $_REQUEST['usingfor'];
	$user_id = $_REQUEST['user_id'];	
	update_user_meta( $user_id, 'i_am_using', $usingfor );
	$user = get_user_by( 'id', $user_id);
	$results_array['sMessage'] = 'success'; // Invalid Email Address
	$results_array['sStatus'] = '0';//1=fail
	$results_array['user_id'] =$user->data->ID;
	$results_array['user_email'] =$user->data->user_email;
	$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
	$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
	$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
	$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
	$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
	$results_array['gender'] = get_user_meta($user_id,'gender',true);
	$results_array['like_refill'] = get_user_meta($user_id,'would_you_like',true);
	$results_array['ask_doctor'] = get_user_meta($user_id,'have_you_ask',true);
	$results_array['usingfor'] = get_user_meta($user_id,'i_am_using',true);
	echo json_encode($results_array);
	exit;
}
elseif($action == "signup5")
{
	$any_allegies = $_REQUEST['any_allegies'];
	$any_medical_conditions = $_REQUEST['any_medical_conditions'];
	$taking_other_medications = $_REQUEST['taking_other_medications'];
	$user_id = $_REQUEST['user_id'];
	update_user_meta( $user_id, 'any_allegies', $any_allegies );
	update_user_meta( $user_id, 'any_medical_conditions', $any_medical_conditions );
	update_user_meta( $user_id, 'taking_other_medications', $taking_other_medications );
	$user = get_user_by( 'id', $user_id);
	$results_array['sMessage'] = 'success'; // Invalid Email Address
	$results_array['sStatus'] = '0';//1=fail
	$results_array['user_id'] =$user->data->ID;
	$results_array['user_email'] =$user->data->user_email;
	$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
	$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
	$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
	$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
	$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
	$results_array['gender'] = get_user_meta($user_id,'gender',true);
	$results_array['like_refill'] = get_user_meta($user_id,'would_you_like',true);
	$results_array['ask_doctor'] = get_user_meta($user_id,'have_you_ask',true);
	$results_array['usingfor'] = get_user_meta($user_id,'i_am_using',true);
	$results_array['any_allegies'] = get_user_meta($user_id,'any_allegies',true);
	$results_array['any_medical_conditions'] = get_user_meta($user_id,'any_medical_conditions',true);
	$results_array['taking_other_medications'] = get_user_meta($user_id,'taking_other_medications',true);
	echo json_encode($results_array);
	exit;	
}
elseif($action == "signup6")
{
	$use_health_insurance = $_REQUEST['use_health_insurance'];
	$user_id = $_REQUEST['user_id'];
	update_user_meta( $user_id, 'use_health_insurance', $use_health_insurance );
	$user = get_user_by( 'id', $user_id);
	$results_array['sMessage'] = 'success'; // Invalid Email Address
	$results_array['sStatus'] = '0';//1=fail
	$results_array['user_id'] =$user->data->ID;
	$results_array['user_email'] =$user->data->user_email;
	$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
	$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
	$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
	$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
	$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
	$results_array['gender'] = get_user_meta($user_id,'gender',true);
	$results_array['like_refill'] = get_user_meta($user_id,'would_you_like',true);
	$results_array['ask_doctor'] = get_user_meta($user_id,'have_you_ask',true);
	$results_array['usingfor'] = get_user_meta($user_id,'i_am_using',true);
	$results_array['any_allegies'] = get_user_meta($user_id,'any_allegies',true);
	$results_array['any_medical_conditions'] = get_user_meta($user_id,'any_medical_conditions',true);
	$results_array['taking_other_medications'] = get_user_meta($user_id,'taking_other_medications',true);
	$results_array['use_health_insurance'] = get_user_meta($user_id,'use_health_insurance',true);
	echo json_encode($results_array);
	exit;
}	
elseif($action == "insurance_card")
{
	$icard = $_REQUEST['image_insurance_card'];
	$user_id = $_REQUEST['user_id'];
	
	
	if (isset($_FILES['image_insurance_card']['name'])) 
	{
		if (!function_exists('wp_generate_attachment_metadata'))
		{
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			require_once(ABSPATH . "wp-admin" . '/includes/file.php');
			require_once(ABSPATH . "wp-admin" . '/includes/media.php');
		}
		$overrides = array( 'test_form' => false);
		$file = wp_handle_upload($_FILES['image_insurance_card'], $overrides);
		$file['url'];
			
		$content = $file['url'];
		
		update_user_meta( $user_id, 'health_insurance_card', $content );
		
		$user = get_user_by( 'id', $user_id);
		$results_array['sMessage'] = 'success'; // Invalid Email Address
		$results_array['sStatus'] = '0';//1=fail
		$results_array['user_id'] =$user->data->ID;
		$results_array['user_email'] =$user->data->user_email;
		$results_array['mobile'] = get_user_meta($user_id,'mobile_number',true);
		$results_array['zip_code'] = get_user_meta($user_id,'zip_code',true);
		$results_array['first_name'] = get_user_meta($user_id,'first_name',true);
		$results_array['last_name'] = get_user_meta($user_id,'last_name',true);
		$results_array['birth_date'] = get_user_meta($user_id,'birth_date',true);
		$results_array['gender'] = get_user_meta($user_id,'gender',true);
		$results_array['like_refill'] = get_user_meta($user_id,'would_you_like',true);
		$results_array['ask_doctor'] = get_user_meta($user_id,'have_you_ask',true);
		$results_array['usingfor'] = get_user_meta($user_id,'i_am_using',true);
		$results_array['any_allegies'] = get_user_meta($user_id,'any_allegies',true);
		$results_array['any_medical_conditions'] = get_user_meta($user_id,'any_medical_conditions',true);
		$results_array['taking_other_medications'] = get_user_meta($user_id,'taking_other_medications',true);
		$results_array['use_health_insurance'] = get_user_meta($user_id,'use_health_insurance',true);
		$results_array['health_insurance_card'] = get_user_meta($user_id,'health_insurance_card',true);
		echo json_encode($results_array);
		exit;
		
	}
	else
	{
		$results_array['sMessage'] = 'File not found.';
		$results_array['sStatus'] = '1';
		echo json_encode($results_array);
		exit;
	}
}
elseif($action == "validzip")
{
	$zipcode = $_GET['zipcode'];
	global $wpdb;
	$zipvali = $wpdb->get_results( 'SELECT location_code FROM wp_woocommerce_shipping_zone_locations WHERE location_type = "postcode"' );
	$zips_all = ""; 
	foreach($zipvali as $zips)
	{
		if($zips_all != "")
			$zips_all = $zips_all.",";
		$zips_all = $zips_all . $zips->location_code;
	}	
	if(in_array($zipcode,explode(',',$zips_all)))
	{
		$results_array['sMessage'] = 'ZIP Code is valid.';
		$results_array['sStatus'] = '0';
		echo json_encode($results_array);
		exit;
	}
	else
	{
		$results_array['sMessage'] = 'ZIP Code is not valid.';
		$results_array['sStatus'] = '1';
		echo json_encode($results_array);
		exit;
	}
}
else
{
	$results_array['sMessage'] = 'Request not found.';
	$results_array['sStatus'] = '1';
	echo json_encode($results_array);
	exit;
}