<?php 
/**
 * Template Name: WebService login
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 * */
?>
<?php 
$email = $usernmae=$_REQUEST['user_email'];
$pass=$_REQUEST['user_pass'];

if(!isset($email) || $email=="")
{
	$results_array['sMessage'] = 'invalid email.';
	$results_array['sStatus'] = '1';
	echo json_encode($results_array);
	exit;
}
elseif(!isset($pass) || $pass=="")
{
	$results_array['sMessage'] = 'invalid password.';
	$results_array['sStatus'] = '1';
	echo json_encode($results_array);
	exit;
}
else
{
	$creds = array();
	$creds['user_login'] = $usernmae;
	$creds['user_password'] =  $pass;
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
	 
	if ( !is_wp_error($user))
	{
	 	$results_array['sMessage'] = 'success';
		$results_array['sStatus'] = '0';	
		$results_array['user_ID'] = $user->data->ID;
		$results_array['user_login'] =$user->data->user_login;
		$results_array['user_email'] =$user->data->user_email;
		$results_array['display_name'] =$user->data->display_name;
		$results_array['mobile'] = get_user_meta($user->data->ID,'mobile_number',true);
		$results_array['zip_code'] = get_user_meta($user->data->ID,'zip_code',true);
		$results_array['first_name'] = get_user_meta($user->data->ID,'first_name',true);
		$results_array['last_name'] = get_user_meta($user->data->ID,'last_name',true);
		$results_array['birth_date'] = get_user_meta($user->data->ID,'birth_date',true);
		$results_array['gender'] = get_user_meta($user->data->ID,'gender',true);
		$results_array['like_refill'] = get_user_meta($user->data->ID,'would_you_like',true);
		$results_array['ask_doctor'] = get_user_meta($user->data->ID,'have_you_ask',true);
		$results_array['usingfor'] = get_user_meta($user->data->ID,'i_am_using',true);
		$results_array['any_allegies'] = get_user_meta($user->data->ID,'any_allegies',true);
		$results_array['any_medical_conditions'] = get_user_meta($user->data->ID,'any_medical_conditions',true);
		$results_array['taking_other_medications'] = get_user_meta($user->data->ID,'taking_other_medications',true);
		$results_array['use_health_insurance'] = get_user_meta($user->data->ID,'use_health_insurance',true);
	}
	else
	{
		$results_array['sMessage'] = 'Login Failed.';
		$results_array['sStatus'] = '1'; //1=fail	
	}
	echo json_encode($results_array);
	exit;
}







