<?php
/**
 * Template Name: WebService Create Order
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage baba
 *
 */
 $userid = get_current_user_id();;
 $prod_id = '83,82';//$_POST['booking_task_id'];
 $cost = $_POST['cost'];
 $stdt = $_POST['booking_start_date'];
 $enddt = $_POST['booking_end_date'];
 $btime = $_POST['booking_time'];
 $time = date('Y-m-d H:i:s');

 $defaults = array(
  'post_status'           => 'pending-confirmation',
  'post_type'             => 'wc_booking',
  'post_author'           => $userid,
  'post_title'            => sprintf( __( 'Booking &ndash; %s'), strftime( _x( '%b %d, %Y @ %I:%M %p') ) ),
  'comment_status'        =>'open',
  'ping_status'           => '',
  'post_parent'           => 0,
  'menu_order'            => 0,
  'to_ping'               =>  '',
  'pinged'                => '',
  'post_password'         => '',
  'guid'                  => '',
  'post_content_filtered' => '',
  'post_excerpt'          => ''
);

$dformat = date("F d,Y",strtotime($bdate));
$order = array(
'post_status'           => 'wc-pending',
  'post_type'             => 'shop_order',
  'post_author'           => $userid,
  'post_title'            => sprintf( __( 'Order &ndash; %s'), strftime( _x( '%b %d, %Y @ %I:%M %p') ) ),
  'comment_status'        =>'open',
  'ping_status'           => '',
  'post_parent'           => 0,
  'menu_order'            => 0,
  'to_ping'               =>  '',
  'pinged'                => '',
  'post_password'         => '',
  'guid'                  => '',
  'post_content_filtered' => '',
  'post_password'	  => uniqid( 'order_' ),
  'post_excerpt'          => ''
);

$oid = wp_insert_post( $order );
$pid = wp_insert_post( $defaults );
 


wc_add_order_item_meta( $item_id, '_qty', 2 );
wc_add_order_item_meta( $item_id1, '_qty', 2 );

$array_product=explode(',',$prod_id);
foreach($array_product as $pid)
{
	$item_id = wc_add_order_item($oid, array(
				 		'order_item_name' 		=> get_the_title($pid),
				 		'order_item_type' 		=> 'line_item'
				 	));
	$price = get_post_meta( $pid, '_regular_price', true);
	wc_add_order_item_meta( $item_id, '_product_id', $price );
	wc_add_order_item_meta( $item_id, '_line_subtotal', $price );
	wc_add_order_item_meta( $item_id, '_line_total', $price);
	wc_add_order_item_meta( $item_id1, '_line_total', $price );
	
	 
}
add_post_meta($oid, '_order_total', 500 );
/* wc_add_order_item_meta( $item_id, '_line_total', 500);
wc_add_order_item_meta( $item_id1, '_line_total', 600 ); */


if(is_wp_error($oid))
{
	$results_array['sMessage'] = 'Booking Not Successful';
	$results_array['sStatus'] = 'false';//1=fail
}
else
{
	$results_array['sMessage'] = 'Booking Successful';
	$results_array['sStatus'] = 'true';//0=success
}
echo json_encode($results_array);
?>
