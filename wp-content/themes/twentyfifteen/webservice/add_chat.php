<?php 
/**
 * Template Name: WebService Add Chat
 * Description: A Page Template
 *
 * @package WordPress
 * @subpackage Baba
 *
 */
global $wpdb;
$user_id=$_REQUEST['user_id'];
$message=$_REQUEST['message'];
$postAuthor='user';
$currentDate=date("Y-m-d H:i:s");
$admid="";
$tablename_chat=$wpdb->prefix.'chat';
$data=array(
	'user_id'    =>$user_id,
	'chat_text' => $message,
	'author' => $postAuthor,
	'date_time' => $currentDate,
	'admin_id' => $admid,
);

	if($wpdb->insert($tablename_chat,$data))
	{
		$results_array['message']=$message;
		$results_array['author']=$postAuthor;
		$results_array['date']=$currentDate;
		$results_array['sMessage'] = 'Message Add Successful';
		$results_array['sStatus'] = 'true';//0=success
	}
	else
	{
		$results_array['sMessage'] = 'Message Not Add Successful';
		$results_array['sStatus'] = 'false';//0=success
	}

echo json_encode($results_array);
?>